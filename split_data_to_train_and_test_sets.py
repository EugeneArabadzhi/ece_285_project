"""
This file splits the data into Test and Train sets.

From: .\..\data_all
To:   .\..\Data\(Test or Train)

CWD: /home/eugene/Desktop/ECE_285/ECE_285_Project
"""

import os
import random
import shutil

cwd = os.getcwd()
test_path = os.path.join(cwd, "..", "Data", "Test")
train_path = os.path.join(cwd, "..", "Data", "Train")
tog_path = os.path.join(cwd, "..", "data_all")

# Split ratio
split_r = 0.8

# Class directory names
lbls = ['neutral', 'anger', 'contempt', 'disgust', 'fear', 'happy', 'sadness', 'surprise']

for lbl in lbls:
    # Path of current class being split
    emo_path = os.path.join(tog_path, lbl)
    # List of images in this class directory
    emo_img_lst = os.listdir(emo_path)
    
    # Get random inds for training set
    num_imgs = len(emo_img_lst)
    num_tr_imgs = int(num_imgs*split_r)
    tr_inds = random.sample(range(num_imgs), num_tr_imgs)
    
    # Move images
    for ind in range(num_imgs):
        cur_path = os.path.join(emo_path, emo_img_lst[ind])
        # Move to Train Set
        if ind in tr_inds:
            new_path = os.path.join(train_path, lbl, emo_img_lst[ind])
        # else move to Test Set
        else:
            new_path = os.path.join(test_path, lbl, emo_img_lst[ind])
        shutil.move(cur_path, new_path)
    
