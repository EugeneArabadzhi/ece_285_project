Description
=============
This project was developed by team "Ron Wayne Venture Capital".
Its members are:

Yevgeniy Arabadzhi

Hugh Runyan

Joel R Martin


Requirements
=============
There are no additional prerequisites, other than those already intalled on UCSD DSMLP.

The directory organization must match that of the BitBucket repository.

Note, the training and testing results might not match those in our report, exactly, but will be close.
The reason for this is the image size reduction we performed in order to fit the 300MB submission  limit. 

Code Organization
=============
split_data_to_train_and_test_sets.py          --  Was used to split/sort data. Grader, do not run this code.

train_and_test_net_v3.ipynb                   --  Main body of code. Run to train and test model. Note, it will train 5 variations of networks, for 25 epochs each. 75 trained networks will be stored in 'Trained Nets and Results' folder. Will take long to train, and will take up a lot of space. This method of training is explained in Section 3.2 of our report.

net_performance_analysis.ipynb                --  RUN THIS. This file will load all the trained networks, that are available, in the 'Trained Nets and Results' folder. It will then plot the accuracy of the classifiers with respect to the epoch number, similar to Figure 9 of our report.
